# Default

Default is the failure to pay interest or principal on a loan or security when due. Default occurs when a debtor is unable to meet the legal obligation of debt repayment, and it also refers to cases in which one party fails to perform on a futures contract as required by an exchange.

---

#### Simplified Definition

Default, or the act of defaulting, means to fail to pay what you owe.  For example if you entered a Cryptoswap and ran out of money to pay the other party on settlements that you lost, you would default.