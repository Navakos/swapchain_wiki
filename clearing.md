# Clearing

In banking and finance, clearing denotes all activities from the time a commitment is made for a transaction until it is settled. This process turns the promise of payment (for example, in the form of a cheque or electronic payment request) into the actual movement of money from one account to another. Clearing houses were formed to facilitate such transactions among banks.

---

#### Simplified Definition

Successfully moving a currency from point A to point B.  For example, if you were to move Bitcoin from Binance to GDAX, the transaction would be cleared once your Bitcoin is accessible and usable in GDAX.