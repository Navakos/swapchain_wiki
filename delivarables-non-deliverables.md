# Deliverables vs. Non-Deliverables

#### Deliverable Swap

Content goes here...

#### Non-Deliverable Swap

A non-deliverable swap (NDS) is a currency swap between major and minor currencies that is restricted or not convertible. A non-deliverable swap is so-called because there is no delivery of the two currencies involved in the swap, unlike a typical currency swap where there is physical exchange of currency flows. Periodic settlement of a NDS is done on a cash basis, generally in U.S. dollars. The settlement value is based on the difference between the exchange rate specified in the swap contract and the spot rate, with one party paying the other the difference. A non-deliverable swap can be viewed as a series of non-deliverable forwards bundled together.

---

#### Simplified Definitions

Content goes here...