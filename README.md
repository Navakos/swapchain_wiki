# Swapchain Knowledgebase

#### How to add pages

- Inside conf.py, add the filename of the page you want to add under the respective section, without the file extension.
- Inside the root directory, add the file with the same filename you created in step one, but with the extension.  (Example: cryptoswaps.md)

#### How to build to production

- Login to readthedocs.io
- Open the 'Swapchain Knowledgebase Project'
- Select 'Overview'
- Select 'Build'

#### Notes

- You can use either .md or .rst files.
- index.rst MUST maintain the .rst extension.