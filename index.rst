Welcome to Swapchain's Knowledgebase!
=======================

Swapchain is a P2P cryptocurrency trading platform. At Swapchain we lower the barrier of entry for everyone from casual crypto fans, tech-savvy millennials, to financial industry experts so everyone can gain control of their financial destiny. The days of day trading and HODLing only are over. Take short positions and profit from the 24-hour volatility of cryptocurrency and obtain financial freedom.

.. toctree::
   :caption: Swapchain Articles
   :maxdepth: 2

   swapchain-explained
   the-cryptoswap
   how-to-create-a-cryptoswap


.. toctree::
   :caption: Cryptocurrency Terms & Definitions
   :maxdepth: 2

   blockchain
   coins
   erc-20
   smart-contracts
   tokens


.. toctree::
   :caption: Financial Terms & Definitions
   :maxdepth: 2

   arbitrage
   clearing
   default
   deliverables-non-deliverables
   derivatives
   fix-rate
   forwards
   futures
   gross-exposure
   hedging
   leverage
   long-short
   market-making
   net-exposure
   options
   premium
   price-base
   speculation
   spot-rate
   spread
   swap-rate
   swaps
   trade-novation