# Arbitrage

In economics and finance, arbitrage is the practice of taking advantage of a price difference between two or more markets: striking a combination of matching deals that capitalize upon the imbalance, the profit being the difference between the market prices.

When used by academics, an arbitrage is a (imagined, hypothetical, thought experiment) transaction that involves no negative cash flow at any probabilistic or temporal state and a positive cash flow in at least one state; in simple terms, it is the possibility of a risk-free profit after transaction costs. For instance, an arbitrage is present when there is the opportunity to instantaneously buy low and sell high.

---

#### Simplified Definition

A currency sold on multiple exchanges may not be listed at the same price on each exchange.  For example, Bitcoin may be listed on Exchange A for $12,000 and listed on Exchange B for $13,000.  Arbitrage, or, having the option to arbitrage would mean that you could buy Bitcoin on Exchange A and sell it instantly on Exchange B for a $1,000 profit.